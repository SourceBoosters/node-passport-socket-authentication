var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var config = require('../../../config/configuration.js');
var url = config.database;

module.exports = function(req, res) {
    // user: { "_id": xxx, "username": yyy }
    var loginUser = {};
    loginUser.username = req.user.username;
    res.json({
        user: loginUser
    });
}
