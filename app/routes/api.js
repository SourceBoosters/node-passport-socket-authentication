var getUser = require('../handlers/user/privateUser.js');
module.exports = function(app, passport, express, io) {
    var api = express.Router();
    
    api.post('/signup', passport.authenticate('local-signup'), function(req, res) {
        res.json({ login: true });
    });
    api.post('/login', passport.authenticate('local-login'), function(req, res) {
        res.json({ login: true });
    });
    api.post('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });
    // From here we can check if user is logged in
    // Then perform next loggedin actions
    api.post('/profile', isLoggedIn, getUser);
    return api;
}
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        return next();
    } else {
        // if they aren't redirect them to the home page
        res.redirect('/');
    }
}
