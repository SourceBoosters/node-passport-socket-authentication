var express = require('express');
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient;
var config = require('./config/configuration.js');
var url = config.database;
var app = express();
var passport = require('passport');
var session = require('express-session');
var http = require('http').Server(app);
var io = require('socket.io')(http);

//connecting to the mongoose db
MongoClient.connect(url, function(err, db) {
    console.log("Connected correctly to server without using mongoose.");
});

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));

// Passport
require('./config/passport')(passport);
app.use(session({
    secret: 'Ec7ud8sJVJ3gw3TB',
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

var api = require('./app/routes/api')(app, passport, express, io);
app.use('/api', api);

app.get('*', function(req, res){
    res.sendFile(__dirname + '/public/app/views/index.html');
});

//the web-app is listening on specific port
http.listen(config.port, function(err) { //http will give us realtime capability
    if (err) {
        console.log(err);
    } else {
        console.log("The web application is listening on port " + config.port);
    }
});
