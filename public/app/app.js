angular.module('MarketApp', ['appRoutes', 'mainCtrl', 'userCtrl', 'userService'])
    .directive('pwCheck', [function() {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                var firstPassword = '#' + attrs.pwCheck;
                elem.append(firstPassword).on('keyup', function() {
                    scope.$apply(function () {
                        ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
                    });
                });
            }
        }
    }]);
