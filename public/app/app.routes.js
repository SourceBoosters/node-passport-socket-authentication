angular.module('appRoutes', ['ngRoute'])

.config(function($routeProvider, $locationProvider) {

    $routeProvider

        .when('/login', {
            templateUrl: 'app/views/pages/login.html'
        })

        .when('/signup', {
            templateUrl: 'app/views/pages/signup.html'
        })

        .when('/', {
            templateUrl: 'app/views/pages/home.html',
            controller: 'UserHomeController',
            controllerAs: 'userHome'
        })

    $locationProvider.html5Mode(true);
});
