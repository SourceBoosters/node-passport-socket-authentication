angular.module('userService', [])

.factory('User', function($http) {
    var userFactory = {};

    userFactory.create = function(userData) {
        return $http.post('/api/signup', userData);
    }
    userFactory.login = function(userData) {
        return $http.post('/api/login', userData);
    }
    userFactory.logout = function() {
        return $http.post('/api/logout');
    }
    userFactory.isLoggedIn = function() {
        return $http.post('/api/isloggedin');
    }
    userFactory.getLoggedUser = function() {
        return $http.post('/api/profile');
    }
    return userFactory;
})
