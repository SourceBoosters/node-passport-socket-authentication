angular.module('mainCtrl', ['mainService', 'userService'])

.controller('MainController', function(User, $rootScope, $location) {
    var vm = this;
    vm.error = {};
    vm.userProfile = {};

    $rootScope.$on('$routeChangeStart', function() {
        User.getLoggedUser()
        .then(function(response) {
            vm.userProfile = response.data.user;
        });
    });
    vm.signupUser = function() {
        User.create(vm.userSignupData)
            .then(function(response) {
                $rootScope.$broadcast('loginSuccess', {
                    login: true
                });
                $location.path('/');
            }, function(error) {
                $location.path('/signup');
            });
    }
    vm.doLogin = function() {
        User.login(vm.loginData)
            .then(function(response) {
                User.getLoggedUser()
                .then(function(response) {
                    vm.userProfile = response.data.user;
                    $rootScope.$broadcast('loginSuccess', {
                        login: true
                    });
                    $location.path('/');
                });
            }, function(error) {
                $location.path('/login');
            });
    }
    vm.doLogout = function() {
        vm.userProfile = {};
        $rootScope.$broadcast('logoutSuccess', {
            login: false
        });
        User.logout();
    }
})
