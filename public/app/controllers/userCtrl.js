angular.module('userCtrl', ['userService'])

.controller('UserController', function(User, $location, $window) {
    var vm = this;
    vm.userProfile = {};
    User.getLoggedUser()
        .then(function(response) {
            vm.userProfile = response.data.user;
        });
})

.controller('UserHomeController', function(User, $scope) {
    var vm = this;
    vm.userProfile = {};
    User.getLoggedUser()
        .then(function(response) {
            vm.userProfile = response.data.user;
        });
    $scope.$on('loginSuccess', function(event, data) {
        User.getLoggedUser()
            .then(function(response) {
                vm.userProfile = response.data.user;
            });
    });
    $scope.$on('logoutSuccess', function(event, data) {
        vm.userProfile = {};
    });
})
