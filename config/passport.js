var LocalStrategy = require('passport-local').Strategy;
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var bcrypt   = require('bcrypt-nodejs');
var config = require('./configuration.js');
var url = config.database;
var newUser = {};

function generateHash(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
function validPassword(password, checkPassword) {
    return bcrypt.compareSync(checkPassword, password);
};

module.exports = function(passport) {
    passport.serializeUser(function(user, done) {
        done(null, user._id);
    });
    passport.deserializeUser(function(_id, done) {
        MongoClient.connect(url, function(err, db) {
            if (err) {
                console.log('Unable to connect to the Server', err);
                return callback(err);
            }
            else {
                var Users = db.collection('users');
                userObjId = new ObjectId(_id);
                Users.findOne({ '_id': userObjId }, { username: 1 }, function(err, user) {
                    done(err, user);
                });
            }
        });
    });

    passport.use('local-signup', new LocalStrategy({ passReqToCallback : true }, function(req, username, password, done) {
        MongoClient.connect(url, function(err, db) {
            if (err) {
                console.log('Unable to connect to the Server', err);
                return done(err);
            }
            else {
                var Users = db.collection('users');
                process.nextTick(function() {
                    Users.findOne({ 'username' :  username }, function(err, user) {
                        if (err) {
                            return done(err);
                        }
                        if (user) {
                            return done(null, false);
                        } else {
                            newUser._id = ObjectId();
                            newUser.username = username;
                            newUser.password = generateHash(password);
                            // save the user
                            Users.insert(newUser, {safe:true}, function(err, result) {
                                if(err) {
                                    console.log(err);
                                    return done(null, false);
                                } else {
                                    return done(null, newUser);
                                }
                            });
                        }
                    });
                });
            }
        });
    }));
    passport.use('local-login', new LocalStrategy({ passReqToCallback: true }, function(req, username, password, done) {
        MongoClient.connect(url, function(err, db) {
            if (err) {
                console.log('Unable to connect to the Server', err);
                return done(err);
            } else {
                var Users = db.collection('users');
                Users.findOne({ 'username': username }, { password: 1 }, function(err, user) {
                    if(err) {
                        return done(err);
                    } else if (!user) {
                        return done(null, false);
                    } else if (!validPassword(user.password, password)) {
                        return done(null, false);
                    } else {
                        return done(null, user);
                    }
                });
            }
        });
    }));
}
